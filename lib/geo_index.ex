defmodule GeoIndex do
  use Bitwise
  require Logger

  def lla_to_ecef({lat, lon, alt}) do
    rad = 6378137.0        # Radius of the Earth (in meters)
    f = 1.0/298.257223563  # Flattening factor WGS84 Model
    cosLat = :math.cos(lat * :math.pi/180)
    sinLat = :math.sin(lat * :math.pi/180)
    cosLon = :math.cos(lon * :math.pi/180)
    sinLon = :math.sin(lon * :math.pi/180)
    oneMinusF = (1.0-f)
    ff = oneMinusF*oneMinusF
    c = 1/ :math.sqrt(cosLat * cosLat + ff* sinLat * sinLat)
    s = c * ff

    cosLon = :math.cos(lon)
    x = (rad * c + alt)*cosLat * cosLon
    y = (rad * c + alt)*cosLat * sinLon
    z = (rad * s + alt)*sinLat

    {x, y, z}
  end

  def octree_hash({x,y,z}) do
    a=8000000
    {ix,iy,iz}={round(x+a),round(y+a),round(z+a)}
    {bx,by,bz}={<< ix :: size(24) >>,<< iy :: size(24) >>,<< iz :: size(24) >>}
    mix_bits(bx,by,bz)
  end

  def mix_bits(x,y,z) do
    mix_bits(x,y,z,"")
  end
  defp mix_bits("","","",acc) do
    acc
  end
  defp mix_bits(x,y,z,acc) do
    << xb :: size(1), xr :: bitstring >> = x
    << yb :: size(1), yr :: bitstring >> = y
    << zb :: size(1), zr :: bitstring >> = z
    mix_bits(xr,yr,zr,<<acc :: bitstring, xb :: size(1), yb :: size(1), zb :: size(1)>>)
  end

  def cell_range(cell_id) do
    s = bit_size(cell_id)
    r = 72-s
    fill = :erlang.bsl(2,r)-1
  #  Logger.info("CE #{inspect r} #{inspect fill}")
    b = << cell_id :: bitstring, 0 :: size(r) >>
    e = << cell_id :: bitstring, fill :: size(r) >>
    {b,e}
  end

  def cells_to_search({xp,yp,zp},maxDist) do
    gbits = :erlang.trunc(:math.log(maxDist) / :math.log(2))+1
    step = 1 <<< gbits
    steps = :erlang.trunc(maxDist/step)+1
    #Logger.debug("GBITS: #{inspect gbits} STEP: #{inspect step} STEPS: #{inspect steps}")
    cells = for x <- -steps .. steps, y <- -steps .. steps, z <- -steps .. steps, do: {xp+x*step, yp+y*step, zp+z*step}
    fbits=72-gbits*3
    cell_ids = for cell <- cells do
      << cell_id :: bitstring-size(fbits), _ :: bitstring >> = octree_hash(cell)
      cell_id
    end
    {cells,cell_ids}
  end

  def distanceSq({x1,y1,z1},{x2,y2,z2}) do
    {xd,yd,zd}={x2-x1,y2-y1,z2-z1}
    xd*xd+yd*yd+zd*zd
  end

  def create_index(ets_name) do
    :ets.new(ets_name,[:public, :named_table, {:keypos,1},:ordered_set])
  end

  def put_ecef(ets_name,ecef,radius,id) do
    key = octree_hash(ecef)
    :ets.insert(ets_name,{key,ecef,radius,id})
  end

  def delete_ecef(ets_name,ecef,id) do
    key = octree_hash(ecef)
    :ets.delete(ets_name,key)
  end

  defp do_scan(ets_name,k,e,acc) do
    nk=:ets.next(ets_name,k)
    if(nk != :"$end_of_table" && nk<=e) do
      do_scan(ets_name,nk,e,:ets.lookup(ets_name,nk) ++ acc)
    else
      acc
    end
  end

  def scan_cell(ets_name,cell_id) do
    {b,e} = cell_range(cell_id)
    do_scan(ets_name,b,e,:ets.lookup(ets_name,b))
  end

  def find(ets_name,pos,maxDist) do
    maxDistSq=maxDist*maxDist
    {_,cell_ids} = cells_to_search(pos,maxDist)
    for cell_id <- cell_ids, {_,ecef,radius,id} <- scan_cell(ets_name,cell_id),
      distanceSq(pos,ecef) <= (maxDist+radius)*(maxDist+radius), do: id
  end
end
