defmodule GeoIndexTest do
  use ExUnit.Case
  require Logger

  setup do
    db = GeoIndex.create_index(:test_geo_index)
    {:ok, [db: db]}
  end

  def random_pos() do
    {:random.uniform(14000000)-7000000,:random.uniform(14000000)-7000000,:random.uniform(14000000)-7000000}
  end

  test "put one place, find it at same place and delete", context do
    db = context.db
    pos=random_pos()
    GeoIndex.put_ecef(db,pos,10,"1")
    res=GeoIndex.find(db,pos,10)
    assert Enum.count(res) == 1
    GeoIndex.delete_ecef(db,pos,"1")
    res=GeoIndex.find(db,pos,10)
    #assert Enum.count(res) == 0
  end

  test "put one place, find it from small distance and delete", context do
    db = context.db
    pos=random_pos()
    {x,y,z} = pos
    npos={x+500, y+500, z}
    GeoIndex.put_ecef(db,pos,10,"1")
    res=GeoIndex.find(db,npos,1000)
    assert Enum.count(res) == 1
    GeoIndex.delete_ecef(db,pos,"1")
    res=GeoIndex.find(db,npos,1000)
    #assert Enum.count(res) == 0
  end

  test "put one place, find it from great distance and delete", context do
    db = context.db
    pos=random_pos()
    {x,y,z} = pos
    npos={x-500000, y-500000, z-500000}
    GeoIndex.put_ecef(db,pos,10,"1")
    res=GeoIndex.find(db,npos,1000000)
    assert Enum.count(res) == 1
    GeoIndex.delete_ecef(db,pos,"1")
    res=GeoIndex.find(db,npos,1000000)
    #assert Enum.count(res) == 0
  end

  @tag timeout: 1_000_000
  test "put and find some places", context do
    db = context.db
    places = for i <- 1 .. 10_000, do: {random_pos(),
      Integer.to_string(i)}
    for place={pos, id} <- places do
      {time,_} = :timer.tc(GeoIndex,:put_ecef,[db,pos,0,id])
    #  Logger.debug("Insert #{inspect id} took #{inspect time} µs")
    end

    for i <- 1 .. 1000 do
      maxDist = i*1000
      maxDistSq=maxDist*maxDist
      pos=random_pos()
      fcnt = Enum.count(places,fn({pp,x}) -> GeoIndex.distanceSq(pos,pp) <= maxDistSq end)
      {time,res}= :timer.tc(GeoIndex, :find, [db,pos,maxDist])
      dbcnt = Enum.count(res)
     # Logger.debug("Search in #{inspect maxDist} meters that found #{inspect dbcnt} of #{inspect fcnt} places took #{inspect time} µs")
      assert fcnt == dbcnt
    end
  end
end
